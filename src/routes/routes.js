import Vue from "vue";
import VueRouter from "vue-router";

import MainLocation from '../components/api/MainPage'
import PanelLogin from '../components/panel/auth/PanelLogin'
import MainDashboard from '../components/panel/dashboard/MainDashboard'
import PanelLocation from "@/components/panel/location/PanelLocation";
import PanelLocationUpdate from "@/components/panel/location/PanelLocationUpdate";
import PanelGames from "@/components/panel/games/PanelGames";
import PanelGamesUpdate from "@/components/panel/games/PanelGamesUpdate";
import PanelGamesUpdateResult from "@/components/panel/games/PanelGamesResult";
import PanelWinners from "@/components/panel/winners/PanelWinners";
import PanelAward from "@/components/panel/award/PanelAward";
import PanelCustomers from "@/components/panel/customers/PanelCustomers";

Vue.use(VueRouter)

const routes = [
    // API ------------------------------------------------------------------------------------
    {path: '/', component: MainLocation},

    // Panel ------------------------------------------------------------------------------------

    // Login ------------------------------------------------------------------------------------
    {path: '/panel/login', component: PanelLogin, name: 'login'},

    // Award ------------------------------------------------------------------------------------
    {
        path: '/panel/award',
        name: 'award',
        component: PanelAward,
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },

    // Locations ------------------------------------------------------------------------------------
    {
        path: '/panel/location',
        name: 'location',
        component: PanelLocation,
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },
    {
        path: '/panel/location/update',
        component: PanelLocationUpdate,
        name: 'locationUpdate',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },

    // Games ------------------------------------------------------------------------------------
    {
        path: '/panel/games',
        component: PanelGames,
        name: 'games',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },
    {
        path: '/panel/games/update',
        component: PanelGamesUpdate,
        name: 'gamesUpdate',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },
    {
        path: '/panel/games/update/result',
        component: PanelGamesUpdateResult,
        name: 'gamesUpdateResult',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },

    // Winners ------------------------------------------------------------------------------------
    {
        path: '/panel/winners',
        component: PanelWinners,
        name: 'winners',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },

    // Customers ------------------------------------------------------------------------------------
    {
        path: '/panel/customers',
        component: PanelCustomers,
        name: 'customers',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    },

    // Dashboard ------------------------------------------------------------------------------------
    {
        path: '/panel/dashboard',
        component: MainDashboard,
        name: 'dashboard',
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('userToken')) {
                next()
            } else {
                next('/panel/login')
            }
        }
    }
];

export default new VueRouter({mode: 'history', routes})
