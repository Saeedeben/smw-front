import Vue from 'vue';
import App from './App.vue';

import router from "./routes/routes";
import {BootstrapVue, IconsPlugin} from "bootstrap-vue";
import axios from "axios";
import VueAxios from "vue-axios";
import store from "./store/store";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

Vue.component('date-picker', VuePersianDatetimePicker);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios, axios)
Vue.use(VuePersianDatetimePicker, {
    name: 'custom-date-picker',
    props: {
        format: 'YYYY-MM-DD HH:mm',
        displayFormat: 'jYYYY-jMM-jDD HH:mm',
        editable: false,
        inputClass: 'form-control my-custom-class-name',
        placeholder: 'Please select a date',
        altFormat: 'YYYY-MM-DD HH:mm',
        color: '#00acc1',
        autoSubmit: true,
    }
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
