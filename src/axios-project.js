import axios from "axios";

const instance = axios.create({
    // baseURL: "http://localhost:8000",
    // baseURL: "http://192.168.1.53:8001",
    // baseURL: "http://192.168.102.36:8001",
    baseURL: "https://worldcup.safarmarket.com",
    // baseURL: "https://safarmarket.com/campaigns/worldcup2022",
})

instance.defaults.headers.common['withCredentials'] = true;

export default instance
